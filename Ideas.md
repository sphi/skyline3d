# Ideas for city architecture
- Skyscrapers (obviously) both normal and weird
- Walkways between skyscrapers
- Space Needle-like tower
- River
- Bridges
- Cranes/construction
- Freeways
- Elevated rail line with moving trains
- Coliseum (underground?)
- City built literally over top of slums
- Billboards with advertisements (Godot/Blender?)
- Windmills
- Airport/conference center/other large building to swing around inside (maybe even giant library?)
- Large aircraft to swing around and try not to fall off of? Zeppelin? Avengers-like Helicarrier?
- Oil rig?
- Parking garage

# TODO before release
## Nontechnical

# Godot
- Indipendent arm movement
- Moving grapple targets
- Tess egg PID tuning
- Stun on hard impact?
- Wire collisions?
- Wire shot arcs?
- More dynamic camera movements?
- Wire recoil?

# Blender
- Fix vertex groups in Tess model
- Tess hair
- More city buildings
- Ground pieces
- Enemy copter

# System
- Export to Android
