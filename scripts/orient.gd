
var proportional_const: float # Pushes toward the target, 2.0 is good
var derivative_const: float # Keeps from over-shooting/hunting, 15.0 is good
var anti_spin_const: float # Dampens unrelated spin, 6.0 is good
var last_tilt_error = 0.0
var target_normal := Vector3(0, 1, 0)
var body: RigidBody

func _init(b: RigidBody, p: float, d: float, s: float):
	body = b
	proportional_const = p
	derivative_const = d
	anti_spin_const = s

func set_target_up(up: Vector3):
	target_normal = up.normalized()
	#last_tilt_error = null

func apply():
	# Calculate how many degrees our up vector is tilted away from the target up vector
	# The clamp() is only required because .dot() can return values slightly out of range, which causes acos to return NAN
	# Fuck floating point errors amiright
	var tilt_error = rad2deg(acos(clamp(body.global_transform.basis.y.dot(target_normal), -1, 1)))
	if last_tilt_error == null:
		last_tilt_error = tilt_error
	# Calculate how much torque to apply to the body based on the current error and the rate of change
	# This is like a PID controller but without the I
	var tilt_nudge = proportional_const * tilt_error + derivative_const * (tilt_error - last_tilt_error)
	last_tilt_error = tilt_error
	# The axis to apply the tilt torque should be perpendicular to both the target normal and the local Y+
	var tilt_axis = body.global_transform.basis.y.cross(target_normal).normalized()
	# This gives us the component of the body's angular velocity that isn't involved in tilt correction
	# Angular velocity should never have trusted tilt axis
	var anti_spin_vec = body.angular_velocity.cross(tilt_axis).cross(tilt_axis)
	# Finally, add everything up
	var torque = tilt_axis * tilt_nudge + anti_spin_vec * anti_spin_const
	# And apply it!
	body.add_torque(torque)
