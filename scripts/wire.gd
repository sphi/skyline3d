extends Spatial

export(NodePath) var body_a;
export(NodePath) var body_b;

var bodies = [null, null]
var anchor_offsets = [Vector3(0, 0, 0), Vector3(0, 0, 0)]
var max_len = null
var min_len = null
var ratchet := false
var pull_force := 0.0
var last_len := 0.0
var current_force := 0.0
var target_speed := 30.0
var current_direction := Vector3(1, 0, 0)
const max_force = 1000

func set_pull_force(value: float):
	pull_force = value

func set_ratchet(value: bool):
	ratchet = value

func get_anchor_positions():
	return [
		bodies[0].global_transform.xform(anchor_offsets[0]),
		bodies[1].global_transform.xform(anchor_offsets[1]),
	]

func _ready():
	if body_a:
		bodies[0] = get_node(body_a)
	if body_b:
		bodies[1] = get_node(body_b)
	for i in range(2):
		assert(bodies[i] is RigidBody)
		bodies[i].connect('integrate_forces', self, 'target_integrate_forces', [i])

func _process(delta: float):
	var anchors = get_anchor_positions()
	global_transform = Transform(Basis(), anchors[0]).looking_at(anchors[1], Vector3(0, 1, 0))
	scale.z = anchors[0].distance_to(anchors[1])

func _physics_process(delta):
	var anchors = get_anchor_positions()
	var rel_vec = (anchors[1] + bodies[1].linear_velocity * delta) - (anchors[0] +  + bodies[0].linear_velocity * delta)
	current_direction = rel_vec.normalized()
	last_len = rel_vec.length()
	if ratchet and (max_len == null || last_len < max_len):
		max_len = last_len
		if min_len:
			max_len = max(max_len, min_len)
	var rel_vel = bodies[1].linear_velocity - bodies[0].linear_velocity
	var wire_speed = rel_vel.dot(current_direction)
	current_force = 0
	if max_len != null and last_len > max_len:
		var target_speed = ((max_len - last_len) / delta) * 0.1
		current_force = ((wire_speed - target_speed) / delta) * 0.5
	if min_len != null and last_len > min_len:
		var factor = clamp(1 + wire_speed / target_speed, 0, 1)
		current_force += pull_force * factor
	current_force = min(current_force, max_force)

func target_integrate_forces(body: PhysicsDirectBodyState, i: int):
	var direction = current_direction
	if i == 1:
		direction *= -1
	#bodies[i].add_force(direction * current_force, anchor_offsets[i])
	bodies[i].add_central_force(direction * current_force)
